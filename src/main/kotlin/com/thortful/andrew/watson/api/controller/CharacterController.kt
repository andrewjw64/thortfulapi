package com.thortful.andrew.watson.api.controller

import com.thortful.andrew.watson.api.controller.response.CharacterResponse
import com.thortful.andrew.watson.api.service.CharacterService
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/characters", produces = ["application/json"])
@Validated
class CharacterController(private val characterService: CharacterService) {

    @GetMapping
    fun searchCharacter(@NotBlank @RequestParam name: String): ResponseEntity<CharacterResponse> {
        LOG.info("Received a character search request with name [$name]")
        return ResponseEntity.ok(characterService.doCharacterSearch(name))
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(CharacterController::class.java)
    }

}