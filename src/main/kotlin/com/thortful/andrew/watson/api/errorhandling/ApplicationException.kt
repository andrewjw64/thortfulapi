package com.thortful.andrew.watson.api.errorhandling

import java.lang.RuntimeException
import org.springframework.http.HttpStatus

abstract class ApplicationException : RuntimeException() {

    abstract val error: ErrorResponse

    abstract val httpStatus: HttpStatus

}