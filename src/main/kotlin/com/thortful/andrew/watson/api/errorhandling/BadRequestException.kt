package com.thortful.andrew.watson.api.errorhandling

import org.springframework.http.HttpStatus

class BadRequestException private constructor( message: String): ApplicationException() {
    override val error  = ErrorResponse(
        code = "bad.request",
        message = message
    )
    override val httpStatus = HttpStatus.BAD_REQUEST

    companion object {
        fun forName(searchTerm: String) =
            BadRequestException("The call to the Star Wars API for [$searchTerm] returned a client error, please check the logs")
    }
}
