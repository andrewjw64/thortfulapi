package com.thortful.andrew.watson.api.client

import com.thortful.andrew.watson.api.starwarsapi.StarWarsAPICharacterResponse
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class StarWarsAPIClient(private val starWarsAPIRestTemplate: RestTemplate) {
    fun doCharacterSearch(searchTerm: String) = starWarsAPIRestTemplate.getForObject(
        "/api/people/?search={searchTerm}",
        StarWarsAPICharacterResponse::class.java,
        searchTerm
    )!!

}
