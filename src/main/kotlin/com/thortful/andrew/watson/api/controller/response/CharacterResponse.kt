package com.thortful.andrew.watson.api.controller.response

data class CharacterResponse(
    val characters: List<CharacterDTO>
)

data class CharacterDTO(
    val name: String,
    val height: String,
    val weight: String,
    val filmAppearances: Int
)
