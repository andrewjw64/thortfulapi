package com.thortful.andrew.watson.api.errorhandling

import org.springframework.http.HttpStatus

class NoSuchCharacterException private constructor(message: String) : ApplicationException() {
    override val error = ErrorResponse(
        code = "no.such.character",
        message = message
    )
    override val httpStatus = HttpStatus.NOT_FOUND

    companion object {
        fun forName(searchTerm: String) =
            NoSuchCharacterException("No results found for search term [$searchTerm]")
    }

}
