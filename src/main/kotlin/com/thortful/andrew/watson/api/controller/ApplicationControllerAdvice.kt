package com.thortful.andrew.watson.api.controller

import com.thortful.andrew.watson.api.errorhandling.ApplicationException
import com.thortful.andrew.watson.api.errorhandling.ErrorResponse
import javax.validation.ConstraintViolationException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MissingServletRequestParameterException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ApplicationControllerAdvice {

    @ExceptionHandler(ApplicationException::class)
    fun handleApplicationException(applicationException: ApplicationException): ResponseEntity<ErrorResponse> {
        LOG.error(applicationException.error.message)
        return ResponseEntity
            .status(applicationException.httpStatus)
            .body(applicationException.error)
    }

    @ExceptionHandler(MissingServletRequestParameterException::class, ConstraintViolationException::class)
    fun handleMissingRequestParam(ex: Exception): ResponseEntity<ErrorResponse> {
        LOG.error("Request failed due to missing request param", ex)
        return ResponseEntity
            .status(HttpStatus.BAD_REQUEST)
            .body(
                ErrorResponse(
                    code = "missing.request.param",
                    message = "Request parameter 'name' is required and must not be empty"
                )
            )
    }

    @ExceptionHandler(Exception::class)
    fun handleUnexpectedException(ex: Exception): ResponseEntity<ErrorResponse> {
        LOG.error(ex.message)
        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(GENERIC_ERROR)
    }

    private companion object {
        val LOG: Logger = LoggerFactory.getLogger(ApplicationControllerAdvice::class.java)
        val GENERIC_ERROR = ErrorResponse(
            code = "unexpected.error",
            message = "These are not the droids you're looking for. Try again later."
        )
    }

}