package com.thortful.andrew.watson.api.errorhandling

import org.springframework.http.HttpStatus

class InternalServerErrorException private constructor(message: String) : ApplicationException() {
    override val error = ErrorResponse(
        code = "internal.server.error",
        message = message
    )
    override val httpStatus = HttpStatus.INTERNAL_SERVER_ERROR

    companion object {
        fun forName(searchTerm: String) = InternalServerErrorException(
            "The call to the Star Wars API for [$searchTerm] returned a server error, please check the logs or try again later"
        )
    }

}
