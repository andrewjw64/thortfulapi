package com.thortful.andrew.watson.api.starwarsapi

data class StarWarsAPICharacterResponse(
    val results: List<StarWarsCharacter>
)

data class StarWarsCharacter(
    val name: String,
    val height: String,
    val mass: String,
    val films: List<String>
)
