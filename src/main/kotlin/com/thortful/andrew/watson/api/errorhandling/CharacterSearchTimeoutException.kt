package com.thortful.andrew.watson.api.errorhandling

import org.springframework.http.HttpStatus

class CharacterSearchTimeoutException private constructor(message: String) : ApplicationException() {

    override val httpStatus = HttpStatus.INTERNAL_SERVER_ERROR
    override val error = ErrorResponse(
        code = "character.search.timed.out",
        message = message
    )

    companion object {
        fun forName(searchTerm: String) =
            CharacterSearchTimeoutException("Unable to search for [$searchTerm], request timed out")
    }
}
