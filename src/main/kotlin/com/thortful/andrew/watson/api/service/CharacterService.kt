package com.thortful.andrew.watson.api.service

import com.thortful.andrew.watson.api.client.StarWarsAPIClient
import com.thortful.andrew.watson.api.controller.response.CharacterDTO
import com.thortful.andrew.watson.api.controller.response.CharacterResponse
import com.thortful.andrew.watson.api.errorhandling.BadRequestException
import com.thortful.andrew.watson.api.errorhandling.CharacterSearchTimeoutException
import com.thortful.andrew.watson.api.errorhandling.InternalServerErrorException
import com.thortful.andrew.watson.api.errorhandling.NoSuchCharacterException
import java.net.SocketTimeoutException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpStatusCodeException
import org.springframework.web.client.ResourceAccessException

@Service
class CharacterService(private val starWarsAPIClient: StarWarsAPIClient) {
    fun doCharacterSearch(name: String): CharacterResponse {
        LOG.info("Star Wars API [Request: $name]")
        val response = tryAndCallStarWarsAPI(name)
        LOG.info("Star Wars API [Request: $name] [Response: $response]")
        if (response.results.isEmpty()) throw NoSuchCharacterException.forName(name)
        return CharacterResponse(
            characters = response.results.map {
                CharacterDTO(
                    name = it.name,
                    height = it.height,
                    weight = it.mass,
                    filmAppearances = it.films.size
                )
            }
        )
    }

    private fun tryAndCallStarWarsAPI(name: String) = try {
        starWarsAPIClient.doCharacterSearch(name)
    } catch (ex: Exception) {
        LOG.error("Call to Star Wars API caused an error", ex)
        when {
            ex is HttpStatusCodeException -> {
                LOG.info("Star Wars API [Request: $name] [Response: ${ex.responseBodyAsString}, Status = ${ex.statusCode}]")
                if (ex is HttpClientErrorException) throw BadRequestException.forName(name)
                else throw InternalServerErrorException.forName(name)
            }
            isATimeout(ex) -> throw CharacterSearchTimeoutException.forName(name)
            else -> throw InternalServerErrorException.forName(name)
        }
    }

    private fun isATimeout(ex: Exception) = ex is ResourceAccessException && ex.rootCause is SocketTimeoutException

    private companion object {
        val LOG: Logger = LoggerFactory.getLogger(CharacterService::class.java)
    }

}
