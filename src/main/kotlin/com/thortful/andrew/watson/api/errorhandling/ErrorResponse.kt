package com.thortful.andrew.watson.api.errorhandling

data class ErrorResponse(
    val code: String,
    val message: String
)