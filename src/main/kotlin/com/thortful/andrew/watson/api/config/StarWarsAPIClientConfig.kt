package com.thortful.andrew.watson.api.config

import java.time.Duration
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class StarWarsAPIClientConfig(
    @Value("\${starwarsapi.url}") private val baseUrl: String,
    @Value("\${starwarsapi.timeoutmillis}") private val timeoutMillis: Long

) {

    @Bean
    fun starWarsAPIRestTemplate(restTemplateBuilder: RestTemplateBuilder): RestTemplate = restTemplateBuilder
        .rootUri(baseUrl)
        .setConnectTimeout(Duration.ofMillis(timeoutMillis))
        .setReadTimeout(Duration.ofMillis(timeoutMillis))
        .build()
}