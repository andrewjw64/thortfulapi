package com.thortful.andrew.watson.api.controller

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/health", produces = ["application/json"])
class HealthController {

    @GetMapping
    fun healthCheck(): ResponseEntity<Void> {
        LOG.info("Received a health check request")
        return ResponseEntity.ok().build()
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(HealthController::class.java)
    }

}