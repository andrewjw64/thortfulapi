package com.thortful.andrew.watson.api.acceptance

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching
import io.restassured.RestAssured.given
import org.hamcrest.Matchers
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension


@ExtendWith(SpringExtension::class)
@ActiveProfiles("acceptance-test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = [WireMockInitializer::class])
class CharacterSearchAcceptanceTest {

    @LocalServerPort
    var localServerPort: Int = 0

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @AfterEach
    fun afterEach() {
        wireMockServer.resetAll()
    }

    @Test
    fun `Should make an outbound request to the Star Wars API when the character search endpoint is called`() {
        stubSearchPeopleRequest()

        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters?name=$LUKE_SKYWALKER")
            .then()
            .statusCode(200)

        wireMockServer.verify(
            WireMock.getRequestedFor(urlPathMatching("/api/people/"))
                .withQueryParam(
                    "search", WireMock.equalTo(LUKE_SKYWALKER)
                )
        )
    }

    @Test
    fun `Should return a timeout error response if call to Star Wars API times out`() {
        stubSearchPeopleRequest(delayInMillis = 10000)
        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters?name=$LUKE_SKYWALKER")
            .then()
            .statusCode(500)
            .body(
                "code", Matchers.equalTo("character.search.timed.out"),
                "message", Matchers.equalTo("Unable to search for [$LUKE_SKYWALKER], request timed out")
            )
    }

    @Test
    fun `Should return a 404 if the call to the Star Wars API returns no results`() {
        val picard = "Jean-Luc Picard"
        stubSearchPeopleRequest(
            responseBody = """{
                    "count": 0, 
                    "next": null, 
                    "previous": null, 
                    "results": []
                }""",
            searchTerm = picard
        )

        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters?name=$picard")
            .then()
            .statusCode(404)
            .body(
                "code", Matchers.equalTo("no.such.character"),
                "message", Matchers.equalTo("No results found for search term [$picard]")
            )


    }

    @Test
    fun `Should return a 400 if we get a 4xx from the call to the Star Wars API`() {
        stubSearchPeopleRequest(status = 400)
        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters?name=$LUKE_SKYWALKER")
            .then()
            .statusCode(400)
            .body(
                "code", Matchers.equalTo("bad.request"),
                "message", Matchers.equalTo("The call to the Star Wars API for [$LUKE_SKYWALKER] returned a client error, please check the logs")
            )
    }

    @Test
    fun `Should return a 500 if we get a 5xx from the call to the Star Wars API`() {
        stubSearchPeopleRequest(status = 500)
        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters?name=$LUKE_SKYWALKER")
            .then()
            .statusCode(500)
            .body(
                "code", Matchers.equalTo("internal.server.error"),
                "message", Matchers.equalTo("The call to the Star Wars API for [$LUKE_SKYWALKER] returned a server error, please check the logs or try again later")
            )
    }

    @Test
    fun `Should return a list of results containing character names, height, weight, and film appearances`(){
        stubSearchPeopleRequest()
        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters?name=$LUKE_SKYWALKER")
            .then()
            .statusCode(200)
            .body(
                "characters[0].name", Matchers.equalTo("Luke Skywalker"),
                "characters[0].height", Matchers.equalTo("172"),
                "characters[0].weight", Matchers.equalTo("77"),
                "characters[0].filmAppearances", Matchers.equalTo(4)
            )

    }

    @Test
    fun `Should throw a 400 for a missing or blank character name request param`(){
        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters")
            .then()
            .statusCode(400)
            .body(
                "code", Matchers.equalTo("missing.request.param"),
                "message", Matchers.equalTo("Request parameter 'name' is required and must not be empty")
            )

        given()
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .get("http://localhost:$localServerPort/api/characters?name=")
            .then()
            .statusCode(400)
            .body(
                "code", Matchers.equalTo("missing.request.param"),
                "message", Matchers.equalTo("Request parameter 'name' is required and must not be empty")
            )

    }

    private fun stubSearchPeopleRequest(
        searchTerm: String = LUKE_SKYWALKER,
        delayInMillis: Int = 0,
        responseBody: String = LUKE_SKYWALKER_JSON,
        status: Int = 200
    ) {
        wireMockServer.stubFor(
            WireMock.get(
                urlPathMatching("/api/people/")
            ).withQueryParam("search", WireMock.equalTo(searchTerm))
                .willReturn(
                    ResponseDefinitionBuilder()
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withStatus(status)
                        .withBody(responseBody)
                        .withFixedDelay(delayInMillis)
                )
        )
    }

    companion object {
        const val LUKE_SKYWALKER = "Luke Skywalker"
        const val LUKE_SKYWALKER_JSON = """{
    "count": 1, 
    "next": null, 
    "previous": null, 
    "results": [
        {
            "name": "Luke Skywalker", 
            "height": "172", 
            "mass": "77", 
            "hair_color": "blond", 
            "skin_color": "fair", 
            "eye_color": "blue", 
            "birth_year": "19BBY", 
            "gender": "male", 
            "homeworld": "http://swapi.dev/api/planets/1/", 
            "films": [
                "http://swapi.dev/api/films/1/", 
                "http://swapi.dev/api/films/2/", 
                "http://swapi.dev/api/films/3/", 
                "http://swapi.dev/api/films/6/"
            ], 
            "species": [], 
            "vehicles": [
                "http://swapi.dev/api/vehicles/14/", 
                "http://swapi.dev/api/vehicles/30/"
            ], 
            "starships": [
                "http://swapi.dev/api/starships/12/", 
                "http://swapi.dev/api/starships/22/"
            ], 
            "created": "2014-12-09T13:50:51.644000Z", 
            "edited": "2014-12-20T21:17:56.891000Z", 
            "url": "http://swapi.dev/api/people/1/"
        }
    ]
}"""
    }
}