package com.thortful.andrew.watson.api.client

import com.thortful.andrew.watson.api.starwarsapi.StarWarsAPICharacterResponse
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.web.client.RestTemplate

@ExtendWith(MockKExtension::class)
internal class StarWarsAPIClientTest {

    @MockK
    lateinit var restTemplate: RestTemplate

    @InjectMockKs
    lateinit var starWarsAPIClient: StarWarsAPIClient

    @Test
    fun `should build path using provided search term and serialise response`() {
        val expected = StarWarsAPICharacterResponse(results = emptyList())
        every { restTemplate.getForObject(any(), StarWarsAPICharacterResponse::class.java, any<String>()) } returns expected
        val actual = starWarsAPIClient.doCharacterSearch("Luke")
        verify { restTemplate.getForObject("/api/people/?search={searchTerm}", StarWarsAPICharacterResponse::class.java, "Luke") }
        assertThat(actual).isEqualTo(expected)
    }
}