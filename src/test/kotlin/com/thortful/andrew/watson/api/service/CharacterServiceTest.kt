package com.thortful.andrew.watson.api.service

import com.thortful.andrew.watson.api.client.StarWarsAPIClient
import com.thortful.andrew.watson.api.errorhandling.BadRequestException
import com.thortful.andrew.watson.api.errorhandling.CharacterSearchTimeoutException
import com.thortful.andrew.watson.api.errorhandling.InternalServerErrorException
import com.thortful.andrew.watson.api.errorhandling.NoSuchCharacterException
import com.thortful.andrew.watson.api.starwarsapi.StarWarsAPICharacterResponse
import com.thortful.andrew.watson.api.starwarsapi.StarWarsCharacter
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import java.net.SocketTimeoutException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.ResourceAccessException

@ExtendWith(MockKExtension::class)
internal class CharacterServiceTest {

    @MockK
    private lateinit var client: StarWarsAPIClient

    @InjectMockKs
    private lateinit var characterService: CharacterService

    @Test
    fun `ResourceAccessExceptions with a underlying cause of a timeout cause a CharacterSearchTimeoutException to be thrown`() {
        every { client.doCharacterSearch(any()) } throws ResourceAccessException("Timeout", SocketTimeoutException())
        assertThrows<CharacterSearchTimeoutException> { characterService.doCharacterSearch(LUKE) }
        verify { client.doCharacterSearch(LUKE) }
    }

    @Test
    fun `Should throw a NoSuchCharacterException if response from client is empty`() {
        every { client.doCharacterSearch(any()) } returns StarWarsAPICharacterResponse(emptyList())
        assertThrows<NoSuchCharacterException> { characterService.doCharacterSearch(LUKE) }
        verify { client.doCharacterSearch(LUKE) }
    }

    @Test
    fun `Should throw a BadRequestException if response from client is a 4xx error`() {
        val ex = HttpClientErrorException(HttpStatus.BAD_REQUEST)
        every { client.doCharacterSearch(any()) } throws ex
        assertThrows<BadRequestException> { characterService.doCharacterSearch(LUKE) }
        verify { client.doCharacterSearch(LUKE) }
    }

    @Test
    fun `Should throw an InternalServerErrorException if response from client is a 5xx error`() {
        val ex = HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR)
        every { client.doCharacterSearch(any()) } throws ex
        assertThrows<InternalServerErrorException> { characterService.doCharacterSearch(LUKE) }
        verify { client.doCharacterSearch(LUKE) }
    }

    @Test
    fun `Should throw an InternalServerErrorException for any other unexpected error from the client`() {
        every { client.doCharacterSearch(any()) } throws Exception("Oh no!")
        assertThrows<InternalServerErrorException> { characterService.doCharacterSearch(LUKE) }
    }

    @Test
    fun `Should transform a non empty response into a CharacterResponse and return it`() {
        val name = "Luke Skywalker"
        val height = "172"
        val mass = "72"
        every { client.doCharacterSearch(any()) } returns StarWarsAPICharacterResponse(
            listOf(
                StarWarsCharacter(
                    name = name,
                    height = height,
                    mass = mass,
                    films = listOf("The first one", "The second one")
                )
            )
        )
        val response = characterService.doCharacterSearch(LUKE)
        assertThat(response.characters).hasSize(1)
        val characterDTO = response.characters[0]
        assertThat(characterDTO.name).isEqualTo(name)
        assertThat(characterDTO.height).isEqualTo(height)
        assertThat(characterDTO.weight).isEqualTo(mass)
        assertThat(characterDTO.filmAppearances).isEqualTo(2)
    }

    private companion object {
        const val LUKE = "Luke"
    }

}