package com.thortful.andrew.watson.api.acceptance

import io.restassured.RestAssured.given
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@ActiveProfiles("acceptance-test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = [WireMockInitializer::class])
class HealthCheckAcceptanceTest {

    @LocalServerPort
    var localServerPort: Int = 0

    @Test
    fun `a health check endpoint exists and always returns 200`(){
        given()
            .get("http://localhost:$localServerPort/health")
            .then()
            .statusCode(200)
    }
}