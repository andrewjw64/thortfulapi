package com.thortful.andrew.watson.api.controller

import com.thortful.andrew.watson.api.errorhandling.CharacterSearchTimeoutException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus

internal class ApplicationControllerAdviceTest {

    private val controllerAdvice = ApplicationControllerAdvice()

    @Test
    fun `Should return a ErrorResponse with the status code from the Exception when handing an ApplicationException`() {
        val ex = CharacterSearchTimeoutException.forName("Darth Vader")
        val response = controllerAdvice.handleApplicationException(ex)
        assertThat(response.statusCode).isEqualTo(ex.httpStatus)
        assertThat(response.body).isEqualTo(ex.error)
    }

    @Test
    fun `Should return a generic 500 error for any non ApplicationException`() {
        val ex = Exception("Oh no!")
        val response = controllerAdvice.handleUnexpectedException(ex)
        assertThat(response.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
        assertThat(response.body?.code).isEqualTo("unexpected.error")
        assertThat(response.body?.message).isEqualTo("These are not the droids you're looking for. Try again later.")
    }
}