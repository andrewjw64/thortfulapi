package com.thortful.andrew.watson.api.acceptance

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.ContextClosedEvent

// https://rieckpil.de/spring-boot-integration-tests-with-wiremock-and-junit-5/
class WireMockInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {
        val wireMockServer = WireMockServer(WireMockConfiguration().dynamicPort())
        wireMockServer.start()
        configurableApplicationContext
            .beanFactory
            .registerSingleton("wireMockServer", wireMockServer)
        configurableApplicationContext.addApplicationListener {
            if (it is ContextClosedEvent) wireMockServer.stop()
        }
        TestPropertyValues
            .of(mapOf("starwarsapi.url" to "http://localhost:${wireMockServer.port()}"))
            .applyTo(configurableApplicationContext)
    }
}