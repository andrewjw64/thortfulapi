This is a simple Spring Boot API that provides an integration with the Star Wars API (http://swapi.dev)

To compile the project and run tests, first run the following:

./gradlew clean build

To run the application, simply run the following command to start the application via the Gradle Wrapper:

./gradlew bootRun

The API provides 2 endpoints:

A health check at GET http://localhost:8080/health
A request to search for a character by name at GET http://localhost:8080/api/characters?name=YourCharacterName